package com.java.homeworks.hw1;


import com.java.homeworks.hw1.service.Launcher;

import java.util.Scanner;

public class GameStart {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Launcher launcher = new Launcher();
        launcher.startGame(scanner);
    }
}
