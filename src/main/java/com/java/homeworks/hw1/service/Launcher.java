package com.java.homeworks.hw1.service;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Launcher {


    public void startGame(Scanner scanner) {
        List<String> players = new Players(scanner).getPlayers();
        Map<String, Integer> playersScore = new ThrowBones(players).getThrow();
        System.out.println("Do you want to retry game? y/n");
        if (scanner.nextLine().equals("y")) {
            players.clear();
            playersScore.clear();
            startGame(scanner);
        }
    }
}
