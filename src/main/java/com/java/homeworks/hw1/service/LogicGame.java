package com.java.homeworks.hw1.service;

import java.util.*;

public class LogicGame {

    private final List<String> playersName = new ArrayList<>();
    private final Map<String, Integer> playersScore = new TreeMap<>();
    Scanner scanner = new Scanner(System.in);

    public void players() {
        System.out.println("Enter count players");
        int count = scanner.nextInt() + 1;
        System.out.println("Enter player's name");
        while (count > 0) {
            playersName.add(scanner.nextLine());
            count--;
        }
    }

    public void throwBones() {
        Random random = new Random();
        for (String s : playersName) {
            playersScore.put(s, random.nextInt(6) + 1);
        }
        playersScore.remove("");
        for (Map.Entry<String, Integer> player : playersScore.entrySet()) {
            System.out.println("Player: " + player.getKey() + " score: " + player.getValue());
        }
        Integer max = Integer.MIN_VALUE;
        for (Map.Entry<String, Integer> player : playersScore.entrySet()) {
            if (player.getValue() > max) {
                max = player.getValue();
            }
        }
        for (Map.Entry<String, Integer> player : playersScore.entrySet()) {
            if (player.getValue().equals(max)) {
                System.out.println("Winner/s: " + player.getKey());
            }
        }
    }

    public void startGame() {
        players();
        throwBones();
        System.out.println("Do you want to retry game? y/n");
        if (scanner.nextLine().equals("y")) {
            playersName.clear();
            playersScore.clear();
            startGame();
        }
    }
}