package com.java.homeworks.hw1.service;

import java.util.*;

public class ThrowBones {

    private final List<String> playersName;
    private final Map<String, Integer> playersScore = new TreeMap<>();

    public ThrowBones(List<String> playersName) {
        this.playersName = playersName;
    }

    public Map<String, Integer> getThrow() {
        Random random = new Random();
        for (String s : playersName) {
            playersScore.put(s, random.nextInt(6) + 1);
        }
        playersScore.remove("");
        for (Map.Entry<String, Integer> player : playersScore.entrySet()) {
            System.out.println("Player: " + player.getKey() + " score: " + player.getValue());
        }
        Integer max = Integer.MIN_VALUE;
        for (Map.Entry<String, Integer> player : playersScore.entrySet()) {
            if (player.getValue() > max) {
                max = player.getValue();
            }
        }
        for (Map.Entry<String, Integer> player : playersScore.entrySet()) {
            if (player.getValue().equals(max)) {
                System.out.println("Winner/s: " + player.getKey());
            }
        }
        return playersScore;
    }
}
