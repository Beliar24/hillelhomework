package com.java.homeworks.hw1.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Players {

    private final List<String> playersName = new ArrayList<>();
    private final Scanner scanner;

    public Players(Scanner scanner) {
        this.scanner = scanner;
    }

    public List<String> getPlayers() {
        System.out.println("Enter count players");
        int count = scanner.nextInt() + 1;
        System.out.println("Enter player's name");
        while (count > 0) {
            playersName.add(scanner.nextLine());
            count--;
        }
        return playersName;
    }
}
